﻿using UnityEngine;
using System.Collections;

public class MousePaintScript : MonoBehaviour {

	public GameObject BackGround;
	public GameObject point20px;

	public bool newNode = true;

	public Vector2 prevPos;

	public GameObject currentNode;

	public Vector2 nowPos;

	public float nodeLenght = 0;

	public int nodeNum = 0;

	public float minX = 10000000;
	public float maxX = -10000000;
	public float minY = 10000000;
	public float maxY = -10000000;

	public GameObject[] LineBounds;

	private bool needReset = true;

	public float boundX = 0;
	public float boundY = 0;

	public float minBoundX = 200;
	public float minBoundY = 200;

	public GameObject TriangleCheckMesh;

	public GameObject[] nodesObjects;

	public int checkResult = 1;

	public Renderer ShapeOK;
	public Renderer ShapeWrong;

	public bool readyToWork = true;

	public LineCheckScript[] lineParts;
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetMouseButton (0) && readyToWork) { // если нажата левая кнопка мышки
			needReset = true;
			nowPos = Input.mousePosition; // считаем координаты курсора мышки (в экранных координатах)
			if (newNode) { // если это новый фрагмент линии
				nodeNum += 1;
				newNode = false;
				if (currentNode) { // во избежании "дыр" в линии , перенастроим ещё раз последний фрагмент линии перед создание нового
					currentNode.transform.LookAt (new Vector3 (nowPos.x - (Screen.width / 2.0f), nowPos.y - (Screen.height / 2.0f), 0));
					nodeLenght = Vector2.Distance (nowPos, prevPos);
					currentNode.transform.localScale = new Vector3 (1, 1, nodeLenght / 20.0f);
					CheckBounds();
				}
				prevPos = nowPos; // запоминаем координта мышки на момент создания нового фрагмента
				currentNode = Instantiate (point20px, point20px.transform.position, point20px.transform.rotation) as GameObject;
				currentNode.name = "node_" + nodeNum;
				currentNode.transform.parent = BackGround.transform;
				currentNode.transform.localPosition = new Vector3 (prevPos.x - (Screen.width / 2.0f), prevPos.y - (Screen.height / 2.0f), 0); // создаём объект в мировых координатах на основе экранных координат курсора мышки
				currentNode.tag = "Player";
				CheckBounds();
			}
			nodeLenght = Vector2.Distance (nowPos, prevPos);
			if (currentNode) {
				currentNode.transform.LookAt (new Vector3 (nowPos.x - (Screen.width / 2.0f), nowPos.y - (Screen.height / 2.0f), 0));
				currentNode.transform.localScale = new Vector3 (1, 1, nodeLenght / 20.0f);
			}
			if (nodeLenght >= 20.0f) { newNode = true; }
			CheckBounds();
		} else { // остановка рисования текущей фигуры
			if (needReset) {
				needReset = false;
				ShapeOK.enabled = false;
				ShapeWrong.enabled = false;
				if (currentNode) StartCoroutine(ResetFigure());
			}
		}
	}

	void CheckBounds () {
		if (currentNode) {
			if (currentNode.transform.localPosition.x <= minX) minX = currentNode.transform.localPosition.x;
			if (currentNode.transform.localPosition.x >= maxX) maxX = currentNode.transform.localPosition.x;
			if (currentNode.transform.localPosition.y <= minY) minY = currentNode.transform.localPosition.y;
			if (currentNode.transform.localPosition.y >= maxY) maxY = currentNode.transform.localPosition.y;
		}
	}

	IEnumerator ResetFigure () {
		readyToWork = false;
		currentNode = null;
		// рисуем рабаритыне линии
		boundX = maxX - minX;
		boundY = maxY - minY;
		LineBounds[0].transform.localPosition = new Vector3 ((minX+boundX/2.0f), maxY, -32);
		LineBounds[0].transform.localScale = new Vector3 (boundX, 2, 2);
		LineBounds[1].transform.localPosition = new Vector3 ((minX+boundX/2.0f), minY, -32);
		LineBounds[1].transform.localScale = new Vector3 (boundX, 2, 2);
		LineBounds[2].transform.localPosition = new Vector3 (minX,(minY+boundY/2.0f) , -32);
		LineBounds[2].transform.localScale = new Vector3 (2,boundY , 2);
		LineBounds[3].transform.localPosition = new Vector3 (maxX,(minY+boundY/2.0f) , -32);
		LineBounds[3].transform.localScale = new Vector3 (2,boundY , 2);
		TriangleCheckMesh.transform.localPosition = new Vector3 ((minX+boundX/2.0f), (minY+boundY/2.0f), 0);
		TriangleCheckMesh.transform.localScale = new Vector3 (boundX, boundY, 100.0f);
		//
		nodesObjects = GameObject.FindGameObjectsWithTag("Player");

		checkResult = 1;

		yield return new WaitForSeconds(0.5f);

		for (int i = 0; i < nodesObjects.Length; i++)
		{
			//Debug.Log("node number "+i+" is named "+nodesObjects[i].name);
			if (nodesObjects[i].GetComponent<NodeScript>().collidedName == "LinesCheck") checkResult *=1; else checkResult *=0; //CheckNode();
		}

		if (lineParts[0].collidedAlreary == true) checkResult *=1; else checkResult *=0;
		if (lineParts[1].collidedAlreary == true) checkResult *=1; else checkResult *=0;
		if (lineParts[2].collidedAlreary == true) checkResult *=1; else checkResult *=0;

		lineParts [0].collidedAlreary = false;
		lineParts [1].collidedAlreary = false;
		lineParts [2].collidedAlreary = false;

		if (checkResult == 1) {
			Debug.Log("All OK");
			ShapeOK.enabled = true;
			ShapeWrong.enabled = false;
		} else {
			Debug.Log("Wrong shape");
			ShapeOK.enabled = false;
			ShapeWrong.enabled = true;
		}

		yield return new WaitForSeconds(2.0f);

		for (int iz = 0; iz < nodesObjects.Length; iz++)
		{
			//Debug.Log("node number "+i+" is named "+nodesObjects[i].name);
			if (nodesObjects[iz]) Destroy(nodesObjects[iz]);
		}

		TriangleCheckMesh.transform.localPosition = new Vector3 (-10000, -10000, 0);
		TriangleCheckMesh.transform.localScale = new Vector3 (1, 1, 1);
		ShapeOK.enabled = false;
		ShapeWrong.enabled = false;

		readyToWork = true;
	}

}
