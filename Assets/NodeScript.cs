﻿using UnityEngine;
using System.Collections;

public class NodeScript : MonoBehaviour {

	public string collidedName = "";

	public void CheckNode () {
		//Debug.Log("node is : " + gameObject.name);
	}

	void OnTriggerEnter(Collider other) {
		//Destroy(other.gameObject);
		collidedName = other.gameObject.name;
		//Debug.Log("node " + gameObject.name + " is collided with " + collidedName);
	}

}
